//#define DEBUG_MODE
//#define TEST_FCNS

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "driverlib/sysctl.h"

#include "ES_Port.h"
#include "termio.h"
#include "Servo_Control.h"
#include "PWMTiva.h"

#define clrScrn() 	printf("\x1b[2J")
#define PWM_SERVO_FREQ 50

//-----------------InitServo function--------------------//
//directly from the PWMLibDev sample


// Here we control only channel 3,4,5 for Servo PWM

void InitServo(void){ 
	// Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  // Your hardware initialization function calls go here
	PWM8_TIVA_Init();
  //PWM8_TIVA_SetFreq(PWM_SERVO_FREQ, 0); //according to lecture 12, the basic motor drive, period should be between 18 to 25 ms
	PWM8_TIVA_SetFreq(PWM_SERVO_FREQ, 1);
	PWM8_TIVA_SetFreq(PWM_SERVO_FREQ, 2);
}


//-------------moveToAngle  function------------------------//
//input: angle you want to move it to, and which pin (0 or 1 are default, unless we intialize more) you connect the servo signal to
//angle goes from 0 to 180 degrees
//output: none

void moveToAngle(uint16_t toAngle, uint8_t channel){
  //1ms pulse width is 0 degrees, 2 ms pulse width is 180 degrees
  static uint16_t setPulseWidthTo = 0;
	setPulseWidthTo=  ((2375*toAngle/180)+625); //0.5 ms to 2.4 ms
	PWM8_TIVA_SetPulseWidth(setPulseWidthTo,channel);
}
  

// for testing purposes
#ifdef TEST_FCNS
int main(void){

	SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
			| SYSCTL_XTAL_16MHZ);
  // initialize the timer sub-system and console I/O
  _HW_Timer_Init(ES_Timer_RATE_1mS);
	TERMIO_Init(); //most likely to be intialized elsewhere already
	//clrScrn();

	// When doing testing, it is useful to announce just which program
	// is running.
	#ifdef DEBUG_MODE
	puts("\rStarting PWMTest \r");
	pr intf("%s %s\r\n",__TIME__, __DATE__);
	printf("\r\n\r\r\n");
	#endif
	
	InitServo();

	while(1){
		moveToAngle(0,0);
		printf("at 0 deg\r\n");
		getchar();
		moveToAngle(45,0);
		printf("at 45 deg\r\n");
		getchar();
		moveToAngle(60,0);
		printf("at 60 deg\r\n");
		getchar();
		moveToAngle(90,0);
		printf("at 90 deg\r\n");
		getchar();
		moveToAngle(120,0);
		printf("at 120 deg\r\n");
		getchar();
		moveToAngle(150,0);
		printf("at 150 deg\r\n");
		getchar();
		moveToAngle(170,0);
		printf("at 170 deg\r\n");
		getchar();
		moveToAngle(175,0);
		printf("at 175 deg\r\n");
		getchar();
		moveToAngle(175,0);
		printf("at 180 deg\r\n");
		getchar();
	}

}

#endif

