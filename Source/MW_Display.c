/****************************************************************************
 Module
   MW_Display.c

 Revision
   1.0.1

 Description
   This is the dispaly Module for the microwave oven.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/07/12 00:36 jec      converted to Events & Services Framework Gen2
 02/21/05 16:20 jec      Began coding
****************************************************************************/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "StageSM.h"
#include "ShootSM.h"
#include "SupplySM.h"
#include "MapKeys.h"
#include "MasterVehicle.h"
#include "Location.h"
#include <stdio.h>
#include "LOCMaster.h"
#include "Ultrasonic.h"
void UpdateDisplay( void )
{
		//query all values we are interested in printing
		MasterVehicleState_t MasterState = QueryMasterVehicleSM();
    StagingState_t StagingState = QueryStageSM();
    ShootingState_t ShootingState = QueryShootSM();
    SupplyingState_t SupplyState = QuerySupplySM();
		LocationState_t LocationState = QueryLocationState();
		uint32_t encoder1_count = get_encoder1_count();
		uint32_t encoder2_count = get_encoder2_count();
		uint8_t green_goal = queryGoalGreen();
		uint8_t red_goal = queryGoalRed();
		int num_ball = get_num_ball();
		uint32_t x_real = get_X();
		uint32_t y_real = get_Y();
		uint32_t y_f_ultra = get_Ultrasonic_Y_F();
		uint32_t y_b_ultra = get_Ultrasonic_Y_B();
		double rpm1 = get_RPM_Encoder1();
		double rpm2 = get_RPM_Encoder2();
		uint8_t Requested_Duty = get_Duty();
    printf("\r");
	
		// printing out all the value we queried
		switch ( MasterState)
    {
        case INIT_STATE :    printf("Master Init    "); 		break;
        case IDLE_STATE : printf("Master Idle    "); 				break;
				case STAGING_STATE: printf("Staging        "); 			break;
				case SHOOTING_STATE : printf("Shoot          "); 		break;
				case SUPPLYING_STATE : printf("Supply         "); 	break;
    }
    switch ( StagingState)
    {
        case STAGE_WAITING :    printf("Waiting        "); 	break;
        case STAGE_MOVE_Y : printf("Moving to Y    "); 			break;
				case STAGE_MOVE_X : printf("Moving to X    "); 			break;
				case STAGE_VERIFICATION: printf("Verify Stage   "); break;
    }
    switch ( ShootingState)
    {
        case SHOOT_WAITING :    printf("Waiting        "); 	break;
        case SHOOT_MOVE_Y : printf("Moving to Y    "); 			break;
				case SHOOT_MOVE_X : printf("Moving to X    ");			break;
				case SHOOTING : printf("Shooting       "); 					break;
				case RESET_SHOOTER : printf("Reset Shooter  "); 		break;
    }
    switch ( SupplyState)
    {
        case SUPPLY_WAITING :    printf("Waiting        "); break;
        case SUPPLY_MOVE_Y : printf("Moving to Y    "); 		break;
				case SUPPLY_MOVE_X : printf("Moving to X    "); 		break;
				case PULSE_SUPPLY : printf("Pulse Supply   "); 			break;
    }
		switch ( LocationState)
    {
        case STOP :    printf("Stop           "); 					break;
        case DRIVE_NORTH : printf("Drive North    "); 			break;
				case DRIVE_SOUTH : printf("Drive South    "); 			break;
				case DRIVE_WEST : printf("Drive West     "); 				break;
				case DRIVE_EAST: printf("Drive East     "); 				break;
    }
		printf("%8d %8d %8d %8d", x_real, y_real, y_f_ultra, y_b_ultra);
		printf("      %.1f      %.1f  ", rpm1, rpm2); 
		printf("%8d", Requested_Duty);
		printf("%8d %8d", green_goal, red_goal);
		printf("%8d", num_ball);
		printf("\r");
}

