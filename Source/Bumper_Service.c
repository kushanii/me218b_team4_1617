//#define TEST 
//#define CHECK_DOT_DASH
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
//#include "ES_ShortTimer.h"
//#include "Integration.h"
#include "MasterVehicle.h"
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <string.h> //this comes in handy when dealing with strings such as appending and comparing

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"
#include "ES_Port.h"

#include "BITDEFS.H"

//#include "MorseElement_Service.h"
//#include "DecodeMorse_Service.h"
#include "Bumper_Service.h"
//#include "LCDService.h"

#include "termio.h"

// these times assume a 1.000mS/tick timing
#define ONE_SEC 976
#define DebounceTime 60 //there is a spec in the Button for the max debouce time, but we can be conservative, better check it though.can't really find it....
#define ALL_BITS (0xff<<2)
//define variables




//Pseudo-code for the Button module (a service that implements a state machine)
//Data private to the module: LastButtonState
static uint8_t LastButtonState;
static uint8_t MyPriority;
DebounceButtonState_t CurrentState;

//InitializeButtonDebounce
//Takes a priority number, returns True.
bool InitializeButtonDebounce(uint8_t Priority){
	//Initialize the MyPriority variable with the passed in parameter.
	MyPriority = Priority; 
	//Initialize the port line to monitor the button
	//fine, enable port A in Tiva, set it as input I/O
	//based on Tiva I/O Tutorial
	//HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0; //priority matters, service 0 is likely to be called last, so want to enable port here
	//enable bit 3 (or 4) on Port A, because we already used bit 2 for detecting rising and falling edge, and bit 3 (or 4) is in a good place
	HWREG(GPIO_PORTF_BASE+GPIO_O_DEN) |= GPIO_PIN_1;
	//set it to input by writing a 0 to the bit
	HWREG(GPIO_PORTF_BASE+GPIO_O_DIR) &= (~GPIO_PIN_1);
//Sample the button port pin and use it to initialize LastButtonState
	LastButtonState=( HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & GPIO_PIN_1);
//Set CurrentState to be DEBOUNCING
	CurrentState=Debouncing;
//Start debounce timer (timer posts to ButtonDebounceSM)
	ES_Timer_InitTimer(BUTTON_TIMER,DebounceTime); //ButtonTimer defined in ES_Configure.h, DebounceTime is defined above
//End of InitializeButton (return True)
	return true;
}//End of InitializeButtonDebounce
 

//posting service would be used a lot, so write a separate function
bool PostButtonDebounceService(ES_Event ThisEvent){
	//printf("Post MorseElement called\n\r");
	return ES_PostToService(MyPriority,ThisEvent);
}


//CheckButtonEvents
//Takes no parameters, returns True if an event posted (11/04/11 jec)
//Local ReturnVal = False, CurrentButtonState
 bool CheckButtonEvents(void){
	 bool ReturnVal=false;
	 uint8_t CurrentButtonState;
	 ES_Event EventToReturn;
//Set CurrentButtonState to state read from port pin
	 CurrentButtonState=( HWREG(GPIO_PORTF_BASE+(GPIO_O_DATA + ALL_BITS)) & GPIO_PIN_1);
	 //printf("Current Button State is %u, Last Button State is %u \n\r",CurrentButtonState, LastButtonState);
//If the CurrentButtonState is different from the LastButtonState
	 if (CurrentButtonState != LastButtonState){
	//Set ReturnVal = True
		 ReturnVal=true;
		//If the CurrentButtonState is down
		 if(CurrentButtonState<=0){ //based on my design, the input to Tiva is pulled high, and when the switch is closed, it connects to GND.
			//PostEvent ButtonDown to ButtonDebounce queue
			 EventToReturn.EventType=DBButtonDown;
			 printf("Bumper Down!\n\r");
			 PostButtonDebounceService(EventToReturn);
			 //printf(" In button event checker, after posting button down\n\r");
		 }
		//Else
		 else{
		//PostEvent ButtonUp to ButtonDebounce queue
			 EventToReturn.EventType=DBButtonUp;
			 printf("Bumper Up!\n\r");
			 PostButtonDebounceService(EventToReturn);
			 //printf(" In button event checker, after posting button up \n\r");
		 	 }
		}//Endif
	 //Endif
//Set LastButtonState to the CurrentButtonState
		LastButtonState=CurrentButtonState;

//Return ReturnVal
		return ReturnVal;
	}//End of CheckButtonEvents

//RunButtonDebounceSM (implements a 2-state state machine for debouncing timing)
//The EventType field of ThisEvent will be one of: ButtonUp, ButtonDown, or ES_TIMEOUT
	ES_Event RunButtonDebounceSM(ES_Event ThisEvent){
		ES_Event EventToReturn;
		ES_Event EventToPost;
	//printf("Run ButtonDebounceSM is called\n\r");
//If CurrentState is Debouncing
		if (CurrentState==Debouncing){
			//printf("In Debouncing state \n\r");
				//If EventType is ES_TIMEOUT & parameter is debounce timer number
				if( (ThisEvent.EventType ==ES_TIMEOUT) && (ThisEvent.EventParam==BUTTON_TIMER)){ //I used timer 1, so it's 1 here
				//Set CurrentState to Ready2Sample
					CurrentState=Ready2Sample;
					//printf("Right after setting CurrentState to Ready2Sample\n\r");
				}
			}
		//Else if CurrentState is Ready2Sample
		else if(CurrentState==Ready2Sample){
			//printf("In Ready2Sample state\n\r");
			//If EventType is ButtonUp
			if (ThisEvent.EventType==DBButtonUp){
			//Start debounce timer
				ES_Timer_InitTimer(BUTTON_TIMER, DebounceTime);
			//Set CurrentState to DEBOUNCING
				CurrentState=Debouncing;
			//Post DBButtonUp to MorseElements & DecodeMorse queues
				//EventToPost.EventType=DBButtonUp;m
				//PostMorseElementService(EventToPost);
				//PostDecodeMorseService(EventToPost);
			}//End if
		//If EventType is ButtonDown
			if(ThisEvent.EventType==DBButtonDown){
		//Start debounce timer
				ES_Timer_InitTimer(BUTTON_TIMER, DebounceTime);
		//Set CurrentState to DEBOUNCING
				CurrentState=Debouncing;
		//Post DBButtonDown to MorseElements & DecodeMorse queues
				//EventToPost.EventType=DBButtonDown;
				EventToPost.EventType=BUMPER_PRESSED;
				PostMasterVehicleSM(EventToPost);
				//PostMorseElementService(EventToPost);
				//PostDecodeMorseService(EventToPost);
				//printf("~~~~~~~~~~~~This is after posting BUTTONDOWN to MorseElement and DecodeMorse~~~~~~~~~~~~~~~\n\r");
			}//End if
		}//End Else
//Return ES_NO_EVENT 
		EventToReturn.EventType=ES_NO_EVENT;
		return EventToReturn;
	}//End of RunButtonDebounceSM
	
	//last edit nov 6th, 14:35
	
	