// Based on Prof.Ed's PWMDemo.c code

#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "bitdefs.h"
#include "OwnPWM.h"

// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerMS 40000/32
#define PWMTicksPer0pt1MS 4000/32 //use this when we want to set PWM freq to more than 1000 Hz
// set 200 Hz frequency so 5mS period
#define PeriodInMS 5
// program generator A to go to 1 at rising comare A, 0 on falling compare A  
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
// program generator B to go to 1 at rising comare B, 0 on falling compare B  
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
#define BitsPerNibble 4

// we will use PWM module 0 for this demo and program it for up/down counting
void SS_InitOwnPWM( void ){
  
	// start by enabling the clock to the PWM Module (PWM1)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;

	// enable the clock to Port B  
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

	// Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
    (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
  
	// make sure that the PWM module clock has gotten going
	while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
    ;

	// disable the PWM while initializing
  HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0;

	// program generators to go to 1 at rising compare A/B, 0 on falling compare A/B  
	// GenA_Normal = (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
  HWREG( PWM0_BASE+PWM_O_1_GENA) = GenA_Normal;
	// GenB_Normal = (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
  HWREG( PWM0_BASE+PWM_O_1_GENB) = GenB_Normal;

	// Set the PWM period. Since we are counting both up & down, we initialize
	// the load register to 1/2 the desired total period. We will also program
	// the match compare registers to 1/2 the desired high time  
  HWREG( PWM0_BASE+PWM_O_1_LOAD) = ((PeriodInMS * PWMTicksPerMS))>>1;
  
	// Set the initial Duty cycle on A to 50% by programming the compare value
	// to 1/2 the period to count up (or down). Technically, the value to program
	// should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2 
	// the period, we can skip the subtract 
  HWREG( PWM0_BASE+PWM_O_1_CMPA) = HWREG( PWM0_BASE+PWM_O_1_LOAD)>>1;

	// Set the initial Duty cycle on B to 25% by programming the compare value
	// to Period/2 - Period/8  (75% of the period)
  HWREG( PWM0_BASE+PWM_O_1_CMPB) = (HWREG( PWM0_BASE+PWM_O_1_LOAD)) -
                                   (((PeriodInMS * PWMTicksPerMS))>>3);

	// enable the PWM outputs
  HWREG( PWM0_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM3EN | PWM_ENABLE_PWM2EN);

	// now configure the Port B pins to be PWM outputs
	// start by selecting the alternate function for PB4 & 5
  HWREG(GPIO_PORTB_BASE+GPIO_O_AFSEL) |= (BIT5HI | BIT4HI);

	// now choose to map PWM to those pins, this is a mux value of 4 that we
	// want to use for specifying the function on bits 4 & 5
  HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) = 
    (HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) & 0xff00ffff) + (4<<(5*BitsPerNibble)) +
      (4<<(4*BitsPerNibble));

	// Enable pins 4 & 5 on Port B for digital I/O
	HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT4HI | BIT5HI);
	
	// make pins 4 & 7 on Port B into outputs
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT4HI |BIT5HI);
  
	// set the up/down count mode, enable the PWM generator and make
	// both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE+ PWM_O_1_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE | 
                                    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}

void SS_SetZeroDCA(void){
	// To program 0% DC, simply set the action on Zero to set the output to zero
  HWREG( PWM0_BASE+PWM_O_1_GENA) = PWM_0_GENA_ACTZERO_ZERO;
	// don't forget to restore the proper actions when the DC drops below 100%
	// or rises above 0% 
}
void SS_SetZeroDCB(void){
	// To program 0% DC, simply set the action on Zero to set the output to zero
  HWREG( PWM0_BASE+PWM_O_1_GENB) = PWM_0_GENB_ACTZERO_ZERO;
	// don't forget to restore the proper actions when the DC drops below 100%
	// or rises above 0% 
}
void SS_SetHundredDCA(void){
	// To program 100% DC, simply set the action on Zero to set the output to one, USE GENA INSTEAD OF GENB
  HWREG( PWM0_BASE+PWM_O_1_GENA) = PWM_0_GENA_ACTZERO_ONE;
	// don't forget to restore the proper actions when the DC drops below 100%
	// or rises above 0% 
}
void SS_SetHundredDCB(void){
	// To program 100% DC, simply set the action on Zero to set the output to one, USE GENA INSTEAD OF GENB
  HWREG( PWM0_BASE+PWM_O_1_GENB) = PWM_0_GENB_ACTZERO_ONE;
	// don't forget to restore the proper actions when the DC drops below 100%
	// or rises above 0% 
}

void SS_RestoreDCA(void){
	// To restore the previos DC, simply set the action back to the normal actions
  HWREG( PWM0_BASE+PWM_O_1_GENA) = GenA_Normal;   
}
void SS_RestoreDCB(void){
	// To restore the previos DC, simply set the action back to the normal actions
  HWREG( PWM0_BASE+PWM_O_1_GENB) = GenB_Normal;    
}

void SS_OwnPWM_SetFreq(uint32_t freq){
	//sets the frequency of the PWM
	
	//calculates the period in ms based on the freq first
	uint32_t desPeriodIn0pt1MS = 10000/freq; //do this weird unit to achieve higher frequency (want ticks remain integer)
	// Set the PWM period. Since we are counting both up & down, we initialize
	// the load register to 1/2 the desired total period. We will also program
	// the match compare registers to 1/2 the desired high time  
  HWREG( PWM0_BASE+PWM_O_1_LOAD) = ((desPeriodIn0pt1MS * PWMTicksPer0pt1MS))>>1;
}

void SS_OwnPWM_SetDutyA(uint32_t duty){
	//let's assume we take only from 0 to 100
	
	//handle special cases first
	if (duty==0){
		SS_RestoreDCA();
		SS_SetZeroDCA();
	}
	if (duty==100){
		SS_RestoreDCA();
		SS_SetHundredDCA();
	}
	else {
	SS_RestoreDCA();
  // Set the initial Duty cycle on A to duty% by programming the compare value
  // to Period/2 - ((Period*duty/100)/2 ) (based on the 25% duty cycle example)
  HWREG( PWM0_BASE+PWM_O_1_CMPA) = (HWREG( PWM0_BASE+PWM_O_1_LOAD)) -
                                   ((HWREG( PWM0_BASE+PWM_O_1_LOAD))*duty/100);
	}
}

void SS_OwnPWM_SetDutyB(uint32_t duty){
	//let's assume we take only from 0 to 100

	//handle special cases first
	if (duty==0){
		SS_SetZeroDCB();
	}
	if (duty==100){
		SS_SetHundredDCB();
	}
	else {
		SS_RestoreDCB();
		// Set the initial Duty cycle on A to duty% by programming the compare value
		// to Period/2 - ((Period*duty/100)/2 ) (based on the 25% duty cycle example)
		HWREG( PWM0_BASE+PWM_O_1_CMPB) = (HWREG( PWM0_BASE+PWM_O_1_LOAD)) -
                                   ((HWREG( PWM0_BASE+PWM_O_1_LOAD))*duty/100);			
	}
}
