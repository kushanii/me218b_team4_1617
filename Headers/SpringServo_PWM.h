#ifndef OWNPWM_H
#define OWNPWM_H

#include <stdint.h>

void SS_InitOwnPWM(void);
void SS_SetZeroDCA(void);
void SS_SetZeroDCB(void);
void SS_RestoreDCA(void);
void SS_RestoreDCB(void);
void SS_SetHundredDCA(void);
void SS_SetHundredDCB(void);
void SS_OwnPWM_SetFreq(uint32_t freq);
void SS_OwnPWM_SetDutyA(uint32_t duty);
void SS_OwnPWM_SetDutyB(uint32_t duty);
#endif
