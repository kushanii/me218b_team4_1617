/****************************************************************************
 Location header file for Hierarchical Sate Machines
 ****************************************************************************/

#ifndef LOCATION_H
#define LOCATION_H
#include <stdint.h>
#include <stdbool.h>
#include "OwnPWM.h"

// State definitions for use with the query function
typedef enum { STOP, DRIVE_NORTH, DRIVE_SOUTH, 
DRIVE_WEST, DRIVE_EAST } LocationState_t;

// Public Function Prototypes
void Init_Location(void);
uint32_t get_X(void);
uint32_t get_Y(void);
void move_X(uint32_t target_X);
void move_Y(uint32_t target_Y);
void InputCaptureResponse_Encoder1(void);
void OneShotIntResponse_Encoder1(void);
void InputCaptureResponse_Encoder2(void);
void OneShotIntResponse_Encoder2(void);
void stopMotor(void);
LocationState_t QueryLocationState ( void );
void runMotor (uint8_t mode);
void Init_PWM_port(void);
void set_PWM_Full_Duty(uint32_t input);
uint32_t get_encoder1_count(void);
uint32_t get_encoder2_count(void);
double get_RPM_Encoder1(void);
double get_RPM_Encoder2(void);
void PeriodicIntResponse( void );
void InitPeriodicInt( void );
void StartOneShot_Encoder1(void);
void InitInputCapture_Encoder1( void );
void InitOneShotInt_Encoder1(void);
void StartOneShot_Encoder2(void);
void InitInputCapture_Encoder2( void );
void InitOneShotInt_Encoder2(void);
void Location_Checker(void);
void updateLocation_from_Ultrasonic(void);
uint8_t get_Duty(void);
bool verify_x_location(void);
bool verify_y_location(void);
void set_location_checker_flag(bool input);
void set_going_to_supply_flag(void);
void clear_going_to_supply_flag(void);
#endif /*LOCATION_H */

