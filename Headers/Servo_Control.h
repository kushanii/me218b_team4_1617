#ifndef ServoControl_H
#define ServoControl_H

void InitServo(void);
void moveToAngle(uint16_t toAngle, uint8_t channel);

#endif

