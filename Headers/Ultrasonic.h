/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Ultrasonic_H
#define Ultrasonic_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes

bool InitUltrasonic ( uint8_t Priority );
bool PostUltrasonic( ES_Event ThisEvent );
ES_Event RunUltrasonic( ES_Event ThisEvent );
uint32_t get_Ultrasonic_X(void);
uint32_t get_Ultrasonic_Y_F(void);
uint32_t get_Ultrasonic_Y_B(void);

#endif /* Ultrasonic_H */

