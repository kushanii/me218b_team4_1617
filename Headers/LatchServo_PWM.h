#ifndef OWNPWM_H
#define OWNPWM_H

#include <stdint.h>

void LS_InitOwnPWM( void );

void LS_SetZeroDCA(void);
void LS_SetZeroDCB(void);
void LS_RestoreDCA(void);
void LS_RestoreDCB(void);
void LS_SetHundredDCA(void);
void LS_SetHundredDCB(void);
void LS_OwnPWM_SetFreq(uint32_t freq);
void LS_OwnPWM_SetDutyA(uint32_t duty);
void LS_OwnPWM_SetDutyB(uint32_t duty);
#endif
