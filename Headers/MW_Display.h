/****************************************************************************
 MW_Display header file for displaying relevant data in the state machine
 ****************************************************************************/

#ifndef MW_Display_H
#define MW_Display_H

// Public Function Prototypes
void UpdateDisplay( void );
void InitDisplay( void );
#endif /* MW_Display_H */

