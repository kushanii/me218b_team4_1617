/****************************************************************************
MasterVehicle header file for Hierarchical Sate Machines

 ****************************************************************************/

#ifndef MasterVehicle_H
#define MasterVehicle_H

// State definitions for use with the query function
typedef enum { INIT_STATE, IDLE_STATE, STAGING_STATE, 
SHOOTING_STATE, SUPPLYING_STATE } MasterVehicleState_t ;

// Public Function Prototypes
ES_Event RunMasterVehicleSM( ES_Event CurrentEvent );
void StartMasterVehicleSM ( ES_Event CurrentEvent );
bool PostMasterVehicleSM( ES_Event ThisEvent );
bool InitMasterVehicleSM ( uint8_t Priority );
MasterVehicleState_t QueryMasterVehicleSM ( void );
uint32_t get_Supply_location_x(void);
uint32_t get_Supply_location_y(void);
int get_num_ball(void);
void increment_num_ball(void);
void decrement_num_ball(void);
int get_Team(void);

uint32_t get_Stage_Green_X(uint8_t index);
uint32_t get_Stage_Green_Y(uint8_t index);
uint32_t get_Stage_Red_X(uint8_t index);
uint32_t get_Stage_Red_Y(uint8_t index);
uint32_t get_Shoot_Green_X(uint8_t index);
uint32_t get_Shoot_Green_Y(uint8_t index);
uint32_t get_Shoot_Red_X(uint8_t index);
uint32_t get_Shoot_Red_Y(uint8_t index);

void update_score(uint8_t red_input, uint8_t green_input);
uint8_t get_red_score(void);
uint8_t get_green_score(void);
#endif /*MasterVehicle_H */

