/****************************************************************************
 ShootSM header file for Hierarchical Sate Machines
 ****************************************************************************/

#ifndef SHOOTSM_H
#define SHOOTSM_H

// typedefs for the states
// State definitions for use with the query function
typedef enum { SHOOT_WAITING, SHOOT_MOVE_X, SHOOT_MOVE_Y, SHOOTING, RESET_SHOOTER } ShootingState_t ;

// Public Function Prototypes
ES_Event RunShootSM( ES_Event CurrentEvent );
void StartShootSM ( ES_Event CurrentEvent );
ShootingState_t QueryShootSM ( void );
uint32_t queryShootingCurrentXDestination(void);
uint32_t queryShootingCurrentYDestination(void);
void set_shot_delay_for_last_eighteen(void);
void reset_shot_delay_time(void);
int get_num_ball(void);
#endif /*SHOOTSM_H */

