#ifndef OWNPWM_H
#define OWNPWM_H

#include <stdint.h>

void DS_InitOwnPWM( void );
void DS_SetZeroDCA(void);
void DS_SetZeroDCB(void);
void DS_RestoreDCA(void);
void DS_RestoreDCB(void);
void DS_SetHundredDCA(void);
void DS_SetHundredDCB(void);
void DS_OwnPWM_SetFreq(uint32_t freq);
void DS_OwnPWM_SetDutyA(uint32_t duty);
void DS_OwnPWM_SetDutyB(uint32_t duty);
#endif
