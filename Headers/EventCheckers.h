/****************************************************************************
 Module
     EventCheckers.h
 Description
     header file for the event checking functions
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 14:37 jec      started coding
*****************************************************************************/

#ifndef EventCheckers_H
#define EventCheckers_H
#include "Bumper_Service.h"
#include "Location.h"

// prototypes for event checkers

bool Check4Keystroke(void);
void Location_Checker( void);


#endif /* EventCheckers_H */
