/****************************************************************************
 Supply header file for Hierarchical Sate Machines
 ****************************************************************************/

#ifndef SUPPLYSM_H
#define SUPPLYSM_H

// typedefs for the states
// State definitions for use with the query function
typedef enum { SUPPLY_WAITING, SUPPLY_MOVE_X, SUPPLY_MOVE_Y, PULSE_SUPPLY } SupplyingState_t ;

// Public Function Prototypes
ES_Event RunSupplySM( ES_Event CurrentEvent );
void StartSupplySM ( ES_Event CurrentEvent );
SupplyingState_t QuerySupplySM ( void );
void InitOneShotInt_Supply( void );
void StartOneShot_Supply_10ms( void );
void StartOneShot_Supply_30ms( void );
void OneShotIntResponse_Supply( void );
#endif /*SUPPLYSM_H */

