/****************************************************************************
 Stage header file for Hierarchical Sate Machines
 ****************************************************************************/

#ifndef STAGESM_H
#define STAGESM_H

// typedefs for the states
// State definitions for use with the query function
typedef enum { STAGE_WAITING, STAGE_MOVE_Y, STAGE_MOVE_X, STAGE_VERIFICATION, 
} StagingState_t ;

// Public Function Prototypes
ES_Event RunStageSM( ES_Event CurrentEvent );
void StartStageSM ( ES_Event CurrentEvent );
StagingState_t QueryStageSM ( void );

#endif /*STAGESM_H */

