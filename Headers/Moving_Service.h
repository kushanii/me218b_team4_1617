/****************************************************************************
 Template header file for Hierarchical Sate Machines AKA StateCharts

 ****************************************************************************/

#ifndef MOVING_SERVICE_H
#define MOVING_SERVICE_H

// State definitions for use with the query function
typedef enum { WAITING, MOVE_Y, MOVE_X} MovingServiceState_t ;

// Public Function Prototypes

ES_Event RunMovingServiceSM( ES_Event CurrentEvent );
void StartMovingServiceSM ( ES_Event CurrentEvent );
bool PostMovingServiceSM( ES_Event ThisEvent );
bool InitMovingServiceSM ( uint8_t Priority );

#endif /*TopHSMTemplate_H */


// To do
// 1. Add get target location for x and y
// 2. Include MOVEMENT_ADJUST_TIMER
// 3. Complete the function with asterisk ***
// 4. Write event checker function for update current position
// 5. calculate encoder step and move in that direction or the difference
// 6. modify interrupt response (if reaching encoder count, stop the motor)
